package main

import (
    "github.com/go-chi/chi"
    "net/http"
    "html/template"
    "log"
)

func main() {
    r := chi.NewRouter()

    r.Get("/", func(w http.ResponseWriter, r *http.Request) {
        page, err := template.ParseFiles("index.html")
        if err != nil {
            log.Println(err)
            http.Error(w, err.Error(), 500)
            return
        }

        err = page.Execute(w, nil)
        if err != nil {
            log.Println(err)
            http.Error(w, err.Error(), 500)
            return
        }
    })

    http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))
    http.Handle("/", r)
    http.ListenAndServe(":8080", nil)
}
