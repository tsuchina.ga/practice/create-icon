# create-icon

Git風のiconを適当に作成する、Go言語で書いたプログラム。

少しのHTML以外、すべてGo言語で書いています。


[サンプル](http://tcna.ga:8080)

## 特徴

* SPA
* 通信しない


## SETUP

* golang 1.10
* linux
* dep
* gopherjs
