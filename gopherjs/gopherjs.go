package main

import (
    "github.com/go-humble/router"
    "gitlab.com/tsuchina.ga/practice/create-icon/gopherjs/views"
    "github.com/gopherjs/vecty"
)

func main() {
    // routing
    r := router.New()
    r.ForceHashURL = true

    r.HandleFunc("/", func(context *router.Context) {
        vecty.RenderBody(&views.HomeView{})
    })
    r.HandleFunc("/{uuid}", func(context *router.Context) {
        vecty.RenderBody(&views.HomeView{Uuid: context.Params["uuid"]})
    })
    r.Start()
}
