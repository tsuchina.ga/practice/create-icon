package views

import (
    "bytes"
    "crypto/sha256"
    "encoding/base64"
    "image"
    "image/color"
    "image/jpeg"
    "log"
    "strconv"
    "sync"
    "time"

    "github.com/gopherjs/gopherjs/js"
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/event"
)

type HomeView struct {
    vecty.Core
    Uuid string
    Image string
    once sync.Once
}

func (v *HomeView) Render() vecty.ComponentOrHTML {
    if v.Uuid != "" {
        go v.once.Do(v.createIamge)
    }

    vecty.SetTitle("create-icon")

    return vecty.Tag("body",
        vecty.Tag("ons-page",
            vecty.Tag("ons-toolbar",
                vecty.Tag("div", vecty.Markup(vecty.Class("left"))),
                vecty.Tag("div", vecty.Markup(vecty.Class("center")), vecty.Text("create-icon")),
                vecty.Tag("div", vecty.Markup(vecty.Class("right")),
                    vecty.Tag("ons-toolbar-button",
                        vecty.Markup(
                            event.Click(func(_ *vecty.Event) {
                                js.Global.Get("window").Call("open",
                                    "https://gitlab.com/tsuchina.ga/practice/create-icon")
                            }),
                        ),
                        vecty.Tag("ons-icon", vecty.Markup(
                            vecty.Attribute("icon", "ion-social-github"),
                        ),
                    )),
                ),
            ),
            vecty.Tag("div", vecty.Markup(
                vecty.Class("content"),
                vecty.Style("text-align", "center"),
                vecty.Style("margin", "15px 0px"),
            ),
                vecty.Tag("div", vecty.Tag("ons-input", vecty.Markup(
                    vecty.Style("width", "32em"),
                    vecty.Attribute("type", "text"),
                    vecty.Attribute("modifier", "underbar"),
                    vecty.Attribute("placeholder", "click 'Create' button"),
                    vecty.Attribute("readonly", true),
                    vecty.Attribute("value", v.Uuid),
                    event.Input(func(e *vecty.Event) {
                        v.Uuid = e.Target.Get("value").String()
                    }),
                ))),
                vecty.Tag("div", vecty.Tag("ons-button", vecty.Text("Create"), vecty.Markup(
                    vecty.Attribute("modifier", "outline"),
                    event.Click(func(e *vecty.Event) {
                        hasher := sha256.New()
                        hasher.Write([]byte(strconv.FormatInt(time.Now().UnixNano(), 10)))
                        v.Uuid = base64.RawURLEncoding.EncodeToString(hasher.Sum(nil))
                        js.Global.Get("location").Set("href", "/#/" + v.Uuid)
                    }),
                ))),
                vecty.Tag("div", vecty.Markup(vecty.Style("margin", "15px 0px")),
                    vecty.Tag("img", vecty.Markup(
                        vecty.Attribute("src", v.Image),
                    )),
                ),
            ),
        ),
    )
}

func (v *HomeView) createIamge() {
    datas, err := base64.RawURLEncoding.DecodeString(v.Uuid)
    if err != nil {
        log.Println(err)
        js.Global.Get("location").Set("href", "/#/")
        return
    }

    if len(datas) != 32 {
        log.Println("data is not 32bytes")
        js.Global.Get("location").Set("href", "/#/")
        return
    }

    // 画像データ作成
    img := image.NewRGBA(image.Rect(0, 0, 255, 255))
    sum := 0
    for _, b := range datas {
        sum += int(b)
    }

    for i, b := range datas {
        if int(b) < sum/len(datas) {
            continue
        }

        // 塗りつぶす位置情報
        r := i % 4 // x軸
        q := int(i / 4) // y軸

        for h := 32 * q; h < 32 * (q + 1); h++ {
            // 左側
            for v := 32 * r; v < 32 * (r + 1); v++ {
                img.Set(v, h, color.RGBA{datas[0], datas[1], datas[2], 255})
            }

            // 右側
            for v := 256 - 32 * (r + 1); v < 256 - 32 * r; v++ {
                img.Set(v, h, color.RGBA{datas[0], datas[1], datas[2], 255})
            }
        }
    }

    // binaryをbase64encodeしてstringとして保持して再描画
    buffer := new(bytes.Buffer)
    err = jpeg.Encode(buffer, img, nil)
    if err != nil {
        log.Println(err)
        js.Global.Get("location").Set("href", "/#/")
    } else {
        v.Image = "data:image/jpg;base64," + base64.StdEncoding.EncodeToString(buffer.Bytes())
        vecty.Rerender(v)
    }
}
